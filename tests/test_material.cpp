#include <catch.hpp>
#include <material.hpp>

TEST_CASE( "create material id", "[material]" )
{
    auto testmaterial = material("wheat", 100);
    REQUIRE(testmaterial.get_id() == "M:wheat:100");
}
