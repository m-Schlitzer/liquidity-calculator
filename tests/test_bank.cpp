#include <catch.hpp>
#include <bank.hpp>

auto wallet = bank(500, 100);
TEST_CASE( "bank getter", "[bank]" )
{
    REQUIRE(wallet.get_cash() == 500);
    REQUIRE(wallet.get_debts() == 100);
    REQUIRE(wallet.get_expendable_cash() == 400);
}

TEST_CASE( "update cash", "[bank]" )
{
    REQUIRE_NOTHROW(wallet.update_cash(440));
    REQUIRE(wallet.get_cash() == 940);
    REQUIRE(wallet.get_expendable_cash() == 840);
}

TEST_CASE( "update debts", "[bank]" )
{
    REQUIRE_NOTHROW(wallet.update_debts(320));
    REQUIRE(wallet.get_debts() == 420);
    REQUIRE(wallet.get_expendable_cash() == 520);
}
