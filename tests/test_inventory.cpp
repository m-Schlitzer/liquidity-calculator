#include <catch.hpp>
#include <product.hpp>
#include <inventory.hpp>

TEST_CASE("add articles", "[inventory]")
{
    inventory inv;
    auto testproduct = product("bread", 20);
    REQUIRE_NOTHROW(inv.add_new_article(testproduct));
    REQUIRE(inv.get_amount_of_articles() == 1);
}

TEST_CASE("remove articles", "[inventory]")
{
    inventory inv;
    auto bread = product("bread", 20);
    auto cornbread = product("cornbread", 10);
    auto nuggets = product("nuggets", 20);
    REQUIRE_NOTHROW(inv.add_new_article(bread));
    REQUIRE_NOTHROW(inv.add_new_article(cornbread));
    REQUIRE_NOTHROW(inv.add_new_article(nuggets));
    REQUIRE_NOTHROW(inv.delete_article(cornbread));
    REQUIRE(inv.get_amount_of_articles() == 2);
    REQUIRE_NOTHROW(inv.delete_article("P:bread:20"));
    REQUIRE(inv.get_amount_of_articles() == 1);
}

TEST_CASE("get article", "[inventory]")
{
    inventory inv;
    auto bread = product("bread", 20);
    REQUIRE_NOTHROW(inv.add_new_article(bread));
    auto article = inv.get_article("P:bread:20");
    REQUIRE(article == bread);
}
