#include <catch.hpp>
#include <product.hpp>

TEST_CASE( "create product id", "[product]" )
{
    auto testproduct = product("bread", 20);
    REQUIRE(testproduct.get_id() == "P:bread:20");
}
