# Liquidity Calculator

## THIS PROJECT IS ON HOLD FOR NOW

This tool is meant for managing your inventory and finances. The user interface is chosen as a TUI, so everyone can use it.

## Versions
This tool is still in production. First release will be version 0.1 in approximately February 2018.

## Dependencies

- cmake
- postgresql
- crypto++
- ncurses

## pqxx
To build this project under Fedora, you need to have pqxx installed from their [git repository](https://github.com/jtv/libpqxx).
The version available in the package manager dnf is not up-to-date and will cause compiler errors. 
For updates when this will change see [Issue 10](https://git.alienco.de/m-schlitzer/liquidity-calculator/issues/10).

## clang-format

Use `clang-format style=file foo.cpp -i` to format the whole file!

Otherwise, before commiting it is recommended to use git-clang-format, your changes won't be merged otherwise.
