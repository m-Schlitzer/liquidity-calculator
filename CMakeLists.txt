cmake_minimum_required (VERSION 3.5)
project (liquidity-calc)

include(CheckCXXCompilerFlag)

if(NOT ${CMAKE_CXX_COMPILER_ID} STREQUAL MSVC)
    option(ENABLE_ASAN "Enable address sanitizer." OFF)
endif()
set(SANITIZE_CXXFLAGS)
set(SANITIZE_LDFLAGS)
if(ENABLE_ASAN)
    if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" AND
       "${CMAKE_CXX_COMPILER_VERSION}" VERSION_GREATER "4.0")
        set(_enable_ubsan TRUE)
    elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" AND
           "${CMAKE_CXX_COMPILER_VERSION}" VERSION_GREATER "7.1")
        set(_enable_ubsan TRUE)
    else()
        set(_enable_ubsan FALSE)
    endif()

    if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" OR
       "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
        if(_enable_ubsan)
            list(APPEND SANITIZE_CXXFLAGS
                "-fsanitize=address,undefined -fno-omit-frame-pointer")
            list(APPEND SANITIZE_LDFLAGS
                "-fsanitize=address,undefined")
        else()
            list(APPEND SANITIZE_CXXFLAGS
                "-fsanitize=address -fno-omit-frame-pointer")
            list(APPEND SANITIZE_LDFLAGS
                "-fsanitize=address")
        endif()

    else()
        message(WARNING "Option ENABLE_ASAN only supported with clang and gcc.")
    endif()
endif()


set(GNUCXX_MINIMUM_VERSION "4.8")
set(CLANGCXX_MINIMUM_VERSION "3.5")
set(CXX_STANDARD_TAG "c++17")

# Helper macro. Add -O0 in Debug configuration to prevent any optimization.
# Makes gdb users much happier.
macro(patch_cmake_cxx_debug_flags)
    if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
        set(CMAKE_CXX_FLAGS_DEBUG "-O0 ${CMAKE_CXX_FLAGS_DEBUG}")
    endif()
endmacro()


if(${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
    patch_cmake_cxx_debug_flags()
    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS CLANGCXX_MINIMUM_VERSION)
        message(FATAL_ERROR
            "Minimum required clang++ version: ${CLANGCXX_MINIMUM_VERSION}")
    endif()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=${CXX_STANDARD_TAG}")
    if(ENABLE_LIBCXX)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
    endif()

    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -fcolor-diagnostics")
elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
    patch_cmake_cxx_debug_flags()
    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS GNUCXX_MINIMUM_VERSION)
        message(FATAL_ERROR
            "Minimum required g++ version: ${GNUCXX_MINIMUM_VERSION}")
    endif()

    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.9")
        set(CMAKE_CXX_FLAGS
            "${CMAKE_CXX_FLAGS} -std=c++1y")
    else()
        set(CMAKE_CXX_FLAGS
            "${CMAKE_CXX_FLAGS} -std=${CXX_STANDARD_TAG}")
    endif()

    if(ENABLE_PROFILE)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pg")
    endif()

    # https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wpedantic")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wcast-qual -Wcast-align -Wdisabled-optimization -Wformat=2 -Wlogical-op -Wmissing-include-dirs -Wshadow -Wredundant-decls -Wdouble-promotion -Wold-style-cast")

    if("${CMAKE_CXX_COMPILER_VERSION}" VERSION_GREATER "6.0")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wduplicated-cond")
    endif()

    if("${CMAKE_CXX_COMPILER_VERSION}" VERSION_GREATER "7.0")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wrestrict -Wduplicated-branches")
    endif()

    # Use colorized output on terminal if supported (GCC 4.9 onwards)
    CHECK_CXX_COMPILER_FLAG("-fdiagnostics-color=auto" GCC_HAS_COLOR)
    if("${GCC_HAS_COLOR}")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fdiagnostics-color=auto")
    endif()
elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL "Intel")
    message(WARNING "Intel C++ not supported.")
else()
    message(AUTHOR_WARNING
        "Could not determine compiler ID: ${CMAKE_CXX_COMPILER_ID}")
endif()

find_package(Curses REQUIRED)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
set(calc_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)
set(calc_SOURCES ${calc_INCLUDE_DIR}/database.hpp
    ${calc_INCLUDE_DIR}/inventory.hpp
    ${calc_INCLUDE_DIR}/profile.hpp
    ${calc_INCLUDE_DIR}/product.hpp
    ${calc_INCLUDE_DIR}/material.hpp
    ${calc_INCLUDE_DIR}/bank.hpp
    ${calc_INCLUDE_DIR}/calculator.hpp)
link_directories(/usr/local/lib)
add_executable(liquidity-calc main.cpp ${calc_SOURCES})
target_link_libraries(liquidity-calc ncurses menu panel tinfo cryptopp pqxx)
set_target_properties(liquidity-calc PROPERTIES
    LINKER_LANGUAGE CXX
    COMPILE_FLAGS "${SANITIZE_CXXFLAGS}"
    LINK_FLAGS "${SANITIZE_LDFLAGS}")

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/tests/catch2)
set(catch2_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/tests/catch2)
add_executable(tests ${calc_SOURCES} ${catch2_INCLUDE_DIR}
    tests/main.cpp
    tests/test_bank.cpp
    tests/test_inventory.cpp
    tests/test_product.cpp
    tests/test_material.cpp
    tests/test_profile.cpp)
set_target_properties(tests PROPERTIES
    LINKER_LANGUAGE CXX
    COMPILE_FLAGS "${SANITIZE_CXXFLAGS}"
    LINK_FLAGS "${SANITIZE_LDFLAGS}")
