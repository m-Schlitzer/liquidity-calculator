#pragma once
#define CALC_UI_H 1
#include <bank.hpp>
#include <cryptopp/cryptlib.h>
#include <database.hpp>
#include <inventory.hpp>
#include <material.hpp>
#include <menu.h>
#include <ncurses.h>
#include <product.hpp>
#include <profile.hpp>
#include <stdlib.h>
#include <string.h>

#define NLINES 10
#define NCOLS 40
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
class calculator
{
public:
    void welcome_prompt(profile& user) { welcome_prompt_impl(user); }

    void mainscreen() { mainscreen_impl(); }

    product create_product() { return create_product_impl(); }

    material create_material() { return create_material_impl(); }

private:
    WINDOW* create_newwin(int h, int w, int y, int x)
    {
        WINDOW *local_win;

        local_win = newwin(h, w, y, x);
        box(local_win, 0 , 0);
        wrefresh(local_win);
        return local_win;
    }

    WINDOW* create_menu_window()
    {
        return newwin(10, 40, (LINES - 10) / 2, (COLS - 40) / 2);
    }

    MENU* create_mainmenu(WINDOW* menu_win)
    {
        const char* choices[] = {
            "Products", "Materials", "Finances", "Profile", "Exit", (char*)NULL,
        };
        const char* descriptions[] = {
            "View/edit products", "View/edit materials", "View/edit finances",
            "Edit your profile",  "Press q to exit",     (char*)NULL,
        };
        ITEM** my_items;
        int n_choices;

        // Create items
        n_choices = ARRAY_SIZE(choices);
        my_items = reinterpret_cast<ITEM**>(calloc(n_choices, sizeof(ITEM*)));
        for (int i = 0; i < n_choices; ++i)
        {
            my_items[i] = new_item(choices[i], descriptions[i]);
        }

        // Create menu
        MENU* menu = new_menu(reinterpret_cast<ITEM**>(my_items));

        keypad(menu_win, TRUE);
        set_menu_win(menu, menu_win);
        set_menu_sub(menu, derwin(menu_win, 6, 38, 3, 1));
        set_menu_mark(menu, " * ");

        // Print a border around the menu window and print a title
        box(menu_win, 0, 0);
        print_in_middle(menu_win, 1, 0, 40, "Aliencode Calculator");
        mvwaddch(menu_win, 2, 0, ACS_LTEE);
        mvwhline(menu_win, 2, 1, ACS_HLINE, 38);
        mvwaddch(menu_win, 2, 39, ACS_RTEE);
        mvprintw(LINES - 1, 1,
                 "Use vim keys to browse through the main menu (Press q "
                 "to Exit)");
        refresh();

        post_menu(menu);
        wrefresh(menu_win);
        return menu;
    }

    void destroy_win(WINDOW *local_win)
    {
        wborder(local_win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
        wrefresh(local_win);
        delwin(local_win);
    }

    void print_in_middle(WINDOW* win, int starty, int startx, int width,
                         std::string string)
    {
        int x, y;
        float temp;

        if (win == NULL)
        {
            win = stdscr;
        }
        getyx(win, y, x);
        if (startx != 0)
        {
            x = startx;
        }
        if (starty != 0)
        {
            y = starty;
        }
        if (width == 0)
        {
            width = 80;
        }

        temp = (width - string.length()) / 2;
        x = startx + int(temp);
        mvwprintw(win, y, x, "%s", string.c_str());
        refresh();
    }

    WINDOW* full_window()
    {
        int width = COLS - 1;
        int height = LINES - 1;
        int starty = (LINES - height) / 2;
        int startx = (COLS - width) / 2;

        initscr();
        cbreak();
        keypad(stdscr, TRUE);
        refresh();

        return create_newwin(height, width, starty, startx);
    }

    void choice(ITEM* item)
    {
        const char* name = item_name(item);
        if (strcmp(name, "Materials") == 0)
        {
            create_material();
        }
        if (strcmp(name, "Products") == 0)
        {
            create_product();
        }
        refresh();
    }

    WINDOW* show_logo() { return show_logo_impl(); }

    void welcome_prompt_impl(profile& user);
    void mainscreen_impl();
    product create_product_impl();
    material create_material_impl();
    WINDOW* show_logo_impl();
};

void calculator::welcome_prompt_impl(profile& user)
{
    initscr();
    auto window = full_window();
    start_color();
    init_pair(1, COLOR_WHITE, COLOR_BLUE);
    assume_default_colors(COLOR_WHITE, COLOR_BLUE);
    attron(A_BOLD | COLOR_PAIR(1));

    auto logo = show_logo();
    int height, width;
    getmaxyx(logo, height, width);

    std::string str = "Please enter your profile name: ";
    mvprintw(height * 2, ((COLS - str.length()) / 2), "%s", str.c_str());
    char input[20];
    getnstr(input, 20);
    refresh();
    str = std::string(input);
    user.set_profile_name(str);

    str = "Welcome to your Liquidity Calculator, " + user.get_profile_name() +
          "!";
    mvprintw((height * 2) + 2, ((COLS - str.length()) / 2), "%s", str.c_str());

    getch();
    refresh();
    destroy_win(window);
    destroy_win(logo);
    clear();
    endwin();
}

void calculator::mainscreen_impl()
{
    auto window = full_window();
    refresh();
    WINDOW* menu_win = create_menu_window();
    MENU* menu = create_mainmenu(menu_win);

    start_color();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);

    int c;
    while ((c = wgetch(menu_win)) != 'q')
    {
        switch (c)
        {
        case 106:
            menu_driver(menu, REQ_DOWN_ITEM);
            break;
        case 107:
            menu_driver(menu, REQ_UP_ITEM);
            break;
        case 10:
            endwin();
            destroy_win(menu_win);
            destroy_win(window);
            choice(current_item(menu));
            window = full_window();
            menu_win = create_menu_window();
            menu = create_mainmenu(menu_win);
            break;
        }
        wrefresh(menu_win);
    }
    clear();
    destroy_win(menu_win);
    destroy_win(window);
    endwin();
}

product calculator::create_product_impl()
{
    clear();
    initscr();
    echo();
    auto window = full_window();

    std::string str = "Enter the product name: ";
    mvprintw(LINES / 2, ((COLS - str.length()) / 2), "%s", str.c_str());
    refresh();
    char input[20];
    getnstr(input, 20);
    auto pname = std::string(input);

    str = "Enter the amount per unit: ";
    mvprintw((LINES / 2) + 2, ((COLS - str.length()) / 2), "%s", str.c_str());
    getnstr(input, 20);
    auto pamount = std::atoi(input);
    product p(pname, pamount);
    refresh();

    str = "The product has been added as: " + p.get_id();
    mvprintw((LINES / 2) + 4, ((COLS - str.length()) / 2), "%s", str.c_str());

    getch();
    endwin();
    destroy_win(window);
    clear();
    noecho();
    return p;
}

material calculator::create_material_impl()
{
    clear();
    echo();
    auto window = full_window();

    std::string str = "Enter the material name: ";
    mvprintw(LINES / 2, ((COLS - str.length()) / 2), "%s", str.c_str());
    char input[20];
    getnstr(input, 20);
    auto mname = std::string(input);

    str = "Enter the amount per unit: ";
    mvprintw((LINES / 2) + 2, ((COLS - str.length()) / 2), "%s", str.c_str());
    getnstr(input, 20);
    auto mamount = std::atoi(input);
    material m(mname, mamount);

    str = "The material has been added as: " + m.get_id();
    mvprintw((LINES / 2) + 4, ((COLS - str.length()) / 2), "%s", str.c_str());

    getch();
    noecho();
    endwin();
    destroy_win(window);
    clear();
    return m;
}

WINDOW* calculator::show_logo_impl()
{
    int height = 16;
    int width = 100;
    int starty = 14;
    int startx = (COLS - width) / 2;
    int i = 15;
    std::vector<std::string> logo = {

        "   ###    ##       #### ######## ##    ##  ######   #######  ######## "
        " ########               ",
        "  ## ##   ##        ##  ##       ###   ## ##    ## ##     ## ##     "
        "## ##                     ",
        " ##   ##  ##        ##  ##       ####  ## ##       ##     ## ##     "
        "## ##                     ",
        "##     ## ##        ##  ######   ## ## ## ##       ##     ## ##     "
        "## ######                 ",
        "######### ##        ##  ##       ##  #### ##       ##     ## ##     "
        "## ##                     ",
        "##     ## ##        ##  ##       ##   ### ##    ## ##     ## ##     "
        "## ##                     ",
        "##     ## ######## #### ######## ##    ##  ######   #######  ######## "
        " ########               ",
        " ######     ###    ##        ######  ##     ## ##          ###    "
        "########  #######  ######## ",
        "##    ##   ## ##   ##       ##    ## ##     ## ##         ## ##      "
        "##    ##     ## ##     ##",
        "##        ##   ##  ##       ##       ##     ## ##        ##   ##     "
        "##    ##     ## ##     ##",
        "##       ##     ## ##       ##       ##     ## ##       ##     ##    "
        "##    ##     ## ######## ",
        "##       ######### ##       ##       ##     ## ##       #########    "
        "##    ##     ## ##   ##  ",
        "##    ## ##     ## ##       ##    ## ##     ## ##       ##     ##    "
        "##    ##     ## ##    ## ",
        " ######  ##     ## ########  ######   #######  ######## ##     ##    "
        "##     #######  ##     ##"};

    noecho();
    WINDOW* win = create_newwin(height, width, starty, startx);

    for (const auto logo_str : logo)
    {
        mvprintw(i, ((COLS - 95) / 2), "%s", logo_str.c_str());
        i++;
    }

    refresh();
    endwin();
    echo();
    return win;
}

