#pragma once
#define CALC_PROFILE_H 1

class profile
{
public:
    void set_profile_name(std::string name) { profile_name_ = name; }

    std::string& get_profile_name() { return profile_name_; }

private:
    std::string profile_name_;
};
