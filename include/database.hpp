#pragma once
#define CALC_DATABASE_H 1
#include <pqxx/pqxx>

class database
{
public:
    void init()
    {
      pqxx::connection c;
      pqxx::work transaction(c);
      pqxx::result r = transaction.exec("CREATE TABLE IF NOT EXISTS profiles("
                                        "profile_id uuid not null,"
                                        "passwd char(64) not null,"
                                        "username varchar(20) not null);"
                                        "CREATE TABLE IF NOT EXISTS products("
                                        "product_id varchar(255) not null, "
                                        "profile_id uuid, "
                                        "name varchar(128) not null, "
                                        "amount int, "
                                        "selling_price int, "
                                        "buying_price int, "
                                        "amount_per_unit int not null);"
                                        "CREATE TABLE IF NOT EXISTS materials("
                                        "material_id varchar(255) not null, "
                                        "profile_id uuid, "
                                        "name varchar(128) not null, "
                                        "amount int, "
                                        "buying_price int, "
                                        "amount_per_unit int not null);"
                                        "CREATE TABLE IF NOT EXISTS finances("
                                        "cash int not null, "
                                        "assets int, "
                                        "debts int)");
      transaction.commit();
    }

    void insert_material(std::string id, std::string name, unsigned int amount_per_unit)
    {
        pqxx::connection c;
        pqxx::work transaction(c);
        pqxx::result r = transaction.exec(
         "INSERT INTO materials (material_id, name, amount_per_unit)"
         "VALUES (\'" + id + "\', \'" + name + "\', \'" + std::to_string(amount_per_unit) + "\');");
        transaction.commit();

    }

    void insert_product(std::string id, std::string name, unsigned int amount_per_unit)
    {
        pqxx::connection c;
        pqxx::work transaction(c);
        pqxx::result r = transaction.exec(
         "INSERT INTO products (product_id, name, amount_per_unit)"
         "VALUES (\'" + id + "\', \'" + name + "\', \'" + std::to_string(amount_per_unit) + "\');");
        transaction.commit();
    }
};

static_assert(std::is_copy_constructible<database>::value);
static_assert(std::is_copy_assignable<database>::value);
static_assert(std::is_move_constructible<database>::value);
static_assert(std::is_move_assignable<database>::value);
static_assert(std::is_default_constructible<database>::value);
