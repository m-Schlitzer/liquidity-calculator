#pragma once
#define CALC_MATERIAL_H 1
#include <type_traits>

class material final
{
public:
    material() = default;
    material(std::string name, unsigned int amount_per_unit)
        : name_(std::move(name)), amount_per_unit_(std::move(amount_per_unit))
    {
        std::string id = "M:" + name_ + ":" + std::to_string(amount_per_unit_);
        id_ = id;
#ifdef CALC_DATABASE_H
        database db;
        db.insert_material(id_, name_, amount_per_unit_);
#endif
    }

    void set_buying_price(int price) { buying_price_ = price; }
    const std::string& get_id() { return id_; }
    const std::string& get_name() { return name_; }
    const unsigned int& get_stored_amount() { return stored_amount_; }
    const unsigned int& get_buying_price() { return buying_price_; }
    const unsigned int& get_amount_per_unit() { return amount_per_unit_; }

private:
    std::string id_;
    std::string name_;
    unsigned int buying_price_;
    unsigned int amount_per_unit_;
    unsigned int stored_amount_;
    friend bool operator==(const material&, const material&);
};

static_assert(std::is_copy_constructible<material>::value);
static_assert(std::is_copy_assignable<material>::value);
static_assert(std::is_move_constructible<material>::value);
static_assert(std::is_move_assignable<material>::value);
static_assert(std::is_default_constructible<material>::value);

inline bool operator==(const material& lhs, const material& rhs)
{
    return (lhs.id_ == rhs.id_);
}


