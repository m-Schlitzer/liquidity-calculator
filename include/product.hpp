#pragma once
#define CALC_PRODUCT_H 1
#include <type_traits>

class product
{
public:
    product(std::string name, unsigned int amount_per_unit)
        : name_(std::move(name)), amount_per_unit_(std::move(amount_per_unit))
    {
        std::string id = "P:" + name_ + ":" + std::to_string(amount_per_unit_);
        id_ = id;
#ifdef CALC_DATABASE_H
        database db;
        db.insert_product(id_, name_, amount_per_unit_);
#endif
    }

    void set_selling_price(int price) { selling_price_ = price; }
    void set_buying_price(int price) { buying_price_ = price; }
    const std::string& get_id() { return id_; }
    const std::string& get_name() { return name_; }
    const unsigned int& get_selling_price() { return selling_price_; }
    const unsigned int& get_buying_price() { return buying_price_; }
    const unsigned int& get_production_cost() { return production_cost_; }
    const unsigned int& get_amount_per_unit() { return amount_per_unit_; }

private:
    std::string id_;
    std::string name_;
    unsigned int selling_price_;
    unsigned int buying_price_;
    unsigned int production_cost_;
    unsigned int amount_per_unit_;
    friend bool operator==(const product&, const product&);
};

inline bool operator==(const product& lhs, const product& rhs)
{
    return (lhs.id_ == rhs.id_);
}

static_assert(std::is_copy_constructible<product>::value);
static_assert(std::is_copy_assignable<product>::value);
static_assert(std::is_move_constructible<product>::value);
static_assert(std::is_move_assignable<product>::value);
static_assert(!std::is_default_constructible<product>::value);
