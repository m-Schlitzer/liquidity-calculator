#pragma once
#define CALC_INVENTORY_H 1
#include <map>
#ifndef CALC_PRODUCT_H
#include <product.hpp>
#endif

class inventory
{
public:
    int get_amount_of_articles() { return articles_.size(); }

    void add_new_article(product p)
    {
        auto product_pair = std::make_pair(p.get_id(), p);
        articles_.insert(product_pair);
    }

    void delete_article(product p)
    {
        articles_.erase(p.get_id());
    }

    void delete_article(std::string id)
    {
        articles_.erase(id);
    }

    product& get_article(std::string id) { return articles_.at(id); }

private:
    std::map<std::string, product> articles_;
};

static_assert(std::is_copy_constructible<inventory>::value);
static_assert(std::is_copy_assignable<inventory>::value);
static_assert(std::is_move_constructible<inventory>::value);
static_assert(std::is_move_assignable<inventory>::value);
static_assert(std::is_default_constructible<inventory>::value);
