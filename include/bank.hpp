#pragma once
#define CALC_BANK_H 1
#include <vector>

class bank
{
public:
    bank(int amount, int debts)
        : cash_(std::move(amount)), debts_(std::move(debts))
    {
    }

    void update_cash(int amount) { cash_ = cash_ + amount; };
    void update_debts(int amount) { debts_ = debts_ + amount; };
    unsigned int& get_cash() { return cash_; };
    unsigned int& get_debts() { return debts_; }
    int get_expendable_cash() { return cash_ - debts_; }

private:
    unsigned int cash_;
    unsigned int debts_;
};

static_assert(std::is_copy_constructible<bank>::value);
static_assert(std::is_copy_assignable<bank>::value);
static_assert(std::is_move_constructible<bank>::value);
static_assert(std::is_move_assignable<bank>::value);
static_assert(!std::is_default_constructible<bank>::value);
