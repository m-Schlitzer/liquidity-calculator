#include <calculator.hpp>

int main()
{
    profile user;
    calculator calc;
    database db;
    db.init();
    calc.welcome_prompt(user);
    calc.mainscreen();
    return 0;
}

// vim:et ts=4 sw=4
